import {createContext} from 'react';

// auth context wrapper for pages
const AuthContext = createContext(false);

export default AuthContext;