import CalendarView from "./Views/Calendar/calendar";
import LoginView from "./Views/Login/login";
import NotFound from "./Views/NotFound/notFound";
import TableView from "./Views/Table/Table";
import UploadImage from "./Views/Upload/upload";

const appRoutes = [
    {
        path: "/login",
        name: "Login",
        component: LoginView,
        layout: "/",
        hideInNav: true,
        isPrivate: false
    }, {
        path: "/table",
        name: "Table",
        component: TableView,
        layout: "/",
        hideInNav: true,
        isPrivate: true
    }, {
        path: "/upload",
        name: "Upload",
        component: UploadImage,
        layout: "/",
        hideInNav: true,
        isPrivate: true
    }, {
        path: "/calendar",
        name: "Calendar",
        component: CalendarView,
        layout: "/",
        hideInNav: true,
        isPrivate: true
    }, {
        path: "/*",
        name: "NotFound",
        component: NotFound,
        layout: "/",
        hideInNav: true,
        isPrivate: true
    }
];

export default appRoutes;