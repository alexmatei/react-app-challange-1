import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import useLocalStorage from "use-local-storage";

const textStyle = {
  fontWeight: "bold",
  fontSize: "20px",
} as const;

function createData(
  id: number,
  name: string,
  size: string,
  recognition: number,
  link: string
) {
  return { name, size, recognition, link };
}

const rows = [
  createData(
    1,
    "Bear",
    "944x472",
    6.0,
    "https://tinyjpg.com/images/social/website.jpg"
  ),
  createData(
    2,
    "Minion",
    "630x354",
    9.0,
    "https://www.cleverfiles.com/howto/wp-content/uploads/2018/03/minion.jpg"
  ),
  createData(
    3,
    "Sun",
    "691x711",
    16.0,
    "https://static5.depositphotos.com/1007168/472/i/950/depositphotos_4725473-stock-photo-hot-summer-sun-wearing-shades.jpg"
  ),
  createData(
    4,
    "Plushie",
    "944x708",
    3.7,
    "https://bobdomo.com/kiko/php/topic_thumbnail/3.jpg"
  ),
  createData(
    5, 
    "Cat",
    "481x480",
    2200,
    "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/481px-Cat03.jpg"
  )
];

function TableView() {
  const [images, setImages] = useLocalStorage("images", JSON.stringify([]));
  const tableRows: Array<Object> = JSON.parse(images);
  return (
    <div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={textStyle}>Image name</TableCell>
              <TableCell style={textStyle} align="right">
                Size
              </TableCell>
              <TableCell style={textStyle} align="right">
                Recognition result
              </TableCell>
              <TableCell style={textStyle} align="right">
                Image download link
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.size}</TableCell>
                <TableCell align="right">{row.recognition}</TableCell>
                <TableCell align="right">{row.link}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default TableView;
