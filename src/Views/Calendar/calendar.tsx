import Datepicker from "../../Components/Datepicker/datepicker";
import Datetable from "../../Components/DateTable/datetable";
import styled from "styled-components";
import {useState} from "react";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

const wd = getWindowDimensions();

const CalendarContainer = styled("div")`
  display: block;
  margin-top: calc(${wd.height}px - 560px);
`;

function CalendarView(props: any) {

  const [tableResponse, setTableResponse] = useState([]);

  return (
    <CalendarContainer>
      <Datepicker setTableResponse={setTableResponse}></Datepicker>
      <Datetable tableResponse={tableResponse}></Datetable>
    </CalendarContainer>
  );
}

export default CalendarView;
