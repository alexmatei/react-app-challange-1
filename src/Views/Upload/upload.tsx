import Box from "@material-ui/core/Box/Box";
import Button from "@material-ui/core/Button/Button";
import { useRef, useState } from "react";
import apiInstance from "../../Api/ApiInstance";

function UploadImage() {

  const [imgSource, setImgSource] = useState(
    "https://ucarecdn.com/017162da-e83b-46fc-89fc-3a7740db0a82/-/preview/28x28/"
  );


  const inputImage = useRef<HTMLInputElement | null>(null);
  const onUploadClicked = () => {
    if(inputImage.current != null) {
      inputImage.current.click();
    }
  }

  const onImageFileUpload = (e: any) => {
    if(e.target.files != null) {
      var formData = new FormData();

      formData.append("inputFile", e.target.files[0]);

      try {
        apiInstance.post("/evaluate", imgSource, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }).then((response) => {
           var fileReader = new FileReader();

           fileReader.onloadend = function() {
             if(fileReader.result != null) {
               setImgSource(fileReader.result.toString());
             };
             fileReader.readAsDataURL(e.target.files[0])
           }
        });
      } catch(err: any) {
        console.log(err)
      }
    }
  };

  return (
    <Box
      sx={{
        width: "70vw",
        height: "75vh",
        ml: "auto",
        mr: "auto",
        mt: "4%",
        bgcolor: "#e5e5e5",
      }}
      textAlign="right"
    >
      <Box
        sx={{
          mt: "20px !important",
          mr: "7.5vw",
        }}
      >
        <Button variant="contained" component="label" onClick={onUploadClicked}>
          Upload
          <input type="file" id="fileUpload" onChange={onImageFileUpload} hidden />
        </Button>
      </Box>
      <Box
        sx={{
          width: "55vw",
          height: "50vh",
          ml: "auto",
          mr: "auto",
          mt: "6%",
          bgcolor: "#c4c4c4",
        }}
        textAlign="center"
        alignItems="center"
        justifyContent="center"
        display="flex"
      >
        Image
      </Box>
    </Box>
  );
}

export default UploadImage;
