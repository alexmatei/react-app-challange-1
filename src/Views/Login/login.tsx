import Grid from "@material-ui/core/Grid";
import avatar from "../../Assets/Images/L.jpg";

import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";

import {
    Avatar,
    InputAdornment,
    Link,
    Stack,
    TextField,
    Typography,
} from "@mui/material";

import MailIcon from "@mui/icons-material/Mail";
import LockIcon from "@mui/icons-material/Lock";
import {useState} from "react";
import useAuth from "../../Hooks/useAuth";

export const theme_bac = {
    background: "linear-gradient(#0000CC, #CC00CC)",
    height: "100vh",
    width: "100vw",
};

export const theme2 = {
    background: "#FE6B8B ",
    width: "700px",
    height: "500px",
};

function LoginView(props: any) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleLogin = async () => {
        useAuth.login(email, password).then(response => {
            if(response.token) {
                localStorage.setItem("authToken", response.token);
                localStorage.setItem("currentUser", email)
                props.history.push("/table");
            }
        }).catch(error => {
            alert("Login Error!");
        })
    }

    return (
        <div style={{...theme_bac}}>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
                style={{minHeight: "100vh"}}
            >
                <Grid item xs={9}>
                    <Card variant="outlined" sx={{borderRadius: "1.5rem"}}>
                        <CardContent sx={{width: "45vw"}}>
                            <Stack direction="row" spacing={13}>
                                <Avatar
                                    alt="Remy sharp"
                                    src={avatar}
                                    sx={{boxShadow: 10, width: 200, height: 200, ml: 1, mr: 1}}
                                    style={{alignSelf: "center", justifyContent: "center"}}
                                />

                                <Stack spacing={2}>
                                    <Typography component="h1" variant="h5">
                                        User Login
                                    </Typography>

                                    <TextField
                                        id="outlined-basic"
                                        label="Email-Id"
                                        variant="outlined"
                                        sx={{borderRadius: 300}}
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <MailIcon
                                                        sx={{
                                                            color: "action.active",
                                                            mr: 2,
                                                            my: 2,
                                                            fontSize: 25,
                                                        }}
                                                    />
                                                </InputAdornment>
                                            ),
                                        }}
                                    />

                                    <TextField
                                        id="outlined-basic"
                                        label="Password"
                                        variant="outlined"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <LockIcon
                                                        sx={{
                                                            color: "action.active",
                                                            mr: 2,
                                                            my: 2,
                                                            fontSize: 25,
                                                        }}
                                                    />
                                                </InputAdornment>
                                            ),
                                        }}
                                    />

                                    <Button
                                        variant="contained"
                                        color="success"
                                        onClick={handleLogin}
                                        sx={{
                                            bgcolor: "blue",
                                            color: "black",
                                            borderRadius: "4%",
                                        }}
                                    >
                                        {" "}
                                        Login
                                    </Button>

                                    <Link
                                        href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                                        variant="body2"
                                    >
                                        {"Forgot Username / Password ?"}
                                    </Link>
                                </Stack>
                            </Stack>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
}

export default LoginView;
