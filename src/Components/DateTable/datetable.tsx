import React, {useState} from 'react';
import {Paper, TableContainer} from "@material-ui/core";
import {Table, TableBody, TableCell, TableHead, TableRow} from "@mui/material";
import axiosApiInstance from "../../Api/ApiInstance";
import axios, {AxiosError} from 'axios';


function DatetableDTO(scheduleId: number, startDate: string, endDate: string) {
    return {scheduleId, startDate, endDate};
}

function Datetable(this: any, props: any) {
    const [datetableDataRows, setDatetableDataRows] = useState([]);
    const scheduleData = [DatetableDTO(1, "2022-01-15", "2022-01-16")];

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 928, minHeight: 80, maxHeight: 100, padding: 10}}>
                <TableHead>
                    <TableRow>
                        <TableCell>Schedule Number</TableCell>
                        <TableCell>Start Date</TableCell>
                        <TableCell>End Date</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>{
                    props.tableResponse?.map((scheduleRow: any) => (
                        <TableRow key={scheduleRow.scheduleId}>
                            <TableCell>{scheduleRow.scheduleId}</TableCell>
                            <TableCell>{scheduleRow.startDate}</TableCell>
                            <TableCell>{scheduleRow.endDate}</TableCell>
                        </TableRow>
                    ))
                }
                </TableBody>
            </Table>
        </TableContainer>
    )
};

export default Datetable;