import React, { useState } from "react";
import styled from "styled-components";
import {ReactComponent as ArrowDown} from "./../../Assets/Svg/date-arrow-down.svg"; 
import {ReactComponent as ArrowUp} from "./../../Assets/Svg/date-arrow-up.svg";

const DropdownContainer = styled("div")`
  min-width: 137.5px;
  max-width: 137.5px;
  width: 137.5px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  z-index: 10;
`;
const DropdownHeader = styled("div")`
  display: flex;
  justify-content: center;
  margin: 8px 0 0 20px;
  width: 110px;
  font-weight: 300;
  font-size: 1.15rem;
  color: #fff;
  background: transparent;
  cursor: pointer;

  &::selection {
    background: transparent;
    color: #ffffff95;
  }
`;
const DropdownListContainer = styled("div")`
  border: none;
  position: relative;
  display: flex;
  justify-content: center;
`;
const DropdownList = styled("ul")`
  width: 124px;
  margin: 33.5px 0 0 -100px;
  position: absolute;
  display: inline-block;
  background: transparent;
  text-align: center;
  box-sizing: border-box;
  border: 2px solid #d9e2ec;
  border-top: none;
  background-color: #ffffff90;
  font-size: 1rem;
  font-weight: 500;
  &:first-child {
    padding-top: 0.5em;
  }
  &:last-child {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }
  z-index: 2;
`;
const ListItem = styled("li")`
  color: #243b53;
  list-style: none;
  margin-bottom: 0.8em;
  cursor: pointer;
  margin: 7.5px 0 0 -38.75px;

  &:last-child {
    margin-bottom: 12.5px;
  }
`;

const MONTHS_ARRAY = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export default function MonthDropdown(props : any) {

  const [months] = useState([...MONTHS_ARRAY]);
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(months[(new Date()).getMonth()]);

  const toggling = () => setIsOpen(!isOpen);

  const onOptionClicked = (value: any) => () => {
    setSelectedOption(value);
    props.currentMonth(value);
    setIsOpen(false);
  };

  return (
    <DropdownContainer>
      <DropdownHeader onClick={toggling}>{selectedOption} </DropdownHeader>
      {isOpen && (
        <DropdownListContainer>
          <DropdownList>
            {months.map((month) => (
              <ListItem onClick={onOptionClicked(month)} key={Math.random()}>
                {month}
              </ListItem>
            ))}
          </DropdownList>
        </DropdownListContainer>
      )}
    {!isOpen ? <ArrowDown style={{ height: '13px', width: '13px', margin: '13px 0 0 -10px ' }}></ArrowDown> : <ArrowUp style={{ height: '13px', width: '13px', margin: '13px 0 0 -10px ' }}></ArrowUp>}
    </DropdownContainer>
  );
}
