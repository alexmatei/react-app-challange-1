import React, { useState } from "react";
import styled from "styled-components";
import {ReactComponent as ArrowDown} from "../../Assets/Svg/date-arrow-down.svg";
import {ReactComponent as ArrowUp} from "../../Assets/Svg/date-arrow-up.svg";

const DropdownContainer = styled("div")`
  min-width: 90px;
  max-width: 90px;
  width: 90px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  z-index: 10;
`;
const DropdownHeader = styled("div")`
  display: flex;
  justify-content: center;
  margin: 8px;
  width: 110px;
  font-weight: 300;
  font-size: 1.15rem;
  color: #fff;
  background: transparent;
  cursor: pointer;

  &::selection {
    background: transparent;
    color: #ffffff95;
  }
`;
const DropdownListContainer = styled("div")`
  border: none;
  position: relative;
  display: flex;
  justify-content: center;
`;
const DropdownList = styled("ul")`
  width: 84px;
  height: 200px;
  overflow: hidden;
  overflow-y: scroll;
  margin: 29.5px 0 0 -67px;
  position: absolute;
  display: inline-block;
  background: transparent;
  text-align: center;
  box-sizing: border-box;
  border: 2px solid #d9e2ec;
  border-top: none;
  background-color: #ffffff90;
  font-size: 1rem;
  font-weight: 500;
  &:first-child {
    padding-top: 0.5em;
  }
  &:last-child {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }
  scrollbar-color: #158fa5 transparent;
  scrollbar-width: thin;
  z-index: 2;
`;
const ListItem = styled("li")`
  color: #243b53;
  list-style: none;
  margin-bottom: 0.8em;
  cursor: pointer;
  margin: 7.5px 0 0 -38.75px;

  &:last-child {
    margin-bottom: 12.5px;
  }
`;

function fillYearsRange(start: any = 1900, end: any = 2121, step = 1) {
  const allNumbers = [start, end, step].every(Number.isFinite);

  if (!allNumbers)
    throw new TypeError(
      "The function fillYearsRange() expects only finite numbers as arguments."
    );
  if (step <= 0)
    throw new Error(
      `The selected step ${step} must be a number greater than 0.`
    );
  if (start > end) {
    step = -step;
  }
  const length = Math.floor(Math.abs((end - start) / step)) + 1;

  return Array.from(Array(length), (x, index) => start + index * step);
}

export default function YearDropdown(props: any) {
  const YEARS_ARRAY = fillYearsRange(1921, 2121);
  console.log(YEARS_ARRAY);
  const [years] = useState([...YEARS_ARRAY]);
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState((new Date()).getFullYear());

  const toggling = () => setIsOpen(!isOpen);

  const onOptionClicked = (value: any) => () => {
    setSelectedOption(value);
    props.currentYear(value)
    setIsOpen(false);
  };

  return (
    <DropdownContainer>
      <DropdownHeader onClick={toggling}>{selectedOption}</DropdownHeader>
      {isOpen && (
        <DropdownListContainer>
          <DropdownList>
            {years.map((year) => (
              <ListItem onClick={onOptionClicked(year)} key={Math.random()}>
                {year}
              </ListItem>
            ))}
          </DropdownList>
        </DropdownListContainer>
      )}
      {!isOpen ? <ArrowDown style={{ height: '20px', width: '21px', margin: '5px 10px 0 -15px' }}></ArrowDown> : <ArrowUp style={{ height: '20px', width: '21px', margin: '5px 10px 0 -15px' }}></ArrowUp>}
    </DropdownContainer>
  );
}
