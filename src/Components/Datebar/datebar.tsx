import React, { useState } from "react";
import styled from "styled-components";

const DatebarContainer = styled("div")`
  min-width: 50px;
  max-width: 50px;
  width: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  z-index: 10;
`;
const DatebarHeader = styled("div")`
  display: flex;
  justify-content: center;
  margin: 5px 0 0 10px;
  width: 50px;
  font-weight: 300;
  font-size: 1.15rem;
  color: #fff;
  background: transparent;
  cursor: pointer;

  &::selection {
    background: transparent;
    color: #ffffff95;
  }
`;
const DatebarText = styled("div")`
  position: absolute;
  display: flex;
  justify-content: cneter;
  margin: 22px;
  font-size: 0.7rem;
`;

export default function Datebar(
  { currentDay }: any
) {

  const [selectedOption, setSelectedOption] = useState(currentDay);
 
  return (
  <DatebarContainer>
    <DatebarHeader>{selectedOption}
    <DatebarText>TODAY</DatebarText></DatebarHeader>
    
  </DatebarContainer>
);
}


