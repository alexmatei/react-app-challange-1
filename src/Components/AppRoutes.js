// @ts-nocheck // ts sees error in return changed to js // ts open issue
import { Redirect, Route } from "react-router-dom";

const AppRoutes = ({ component: Component, path, isPrivate, ...rest }) => {
    let userDetails = localStorage.getItem("currentUser");
    if (!userDetails) {
        userDetails = {
            user: {
                email: "",
                password: "",
            },
            token: "none",
        };
    }
    return (
        <Route
            path={path}
            render={(props) =>
                (isPrivate && userDetails.token === "none") ? (
                    <Redirect to={{ pathname: "/login" }} />
                ) : (
                    <Component {...props} />
                )
            }
            {...rest}
        />
    );
};

export default AppRoutes;