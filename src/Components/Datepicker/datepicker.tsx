import React, {useEffect, useState} from "react";
import Datebar from "../Datebar/datebar";
import MonthDropdown from "../Dropdowns/month-dropdown";
import YearDropdown from "../Dropdowns/year-dropdown";
import "./datepicker.scss";

import styled from "styled-components";
import axiosApiInstance from "../../Api/ApiInstance";
import {AxiosError} from "axios";

const ButtonContainer = styled("div")`
  display: flex;
  align-items: center;
  justify-content: center;
`;
const CancelButton = styled("button")`
  border: none;
  border-radius: 20px;
  box-shadow: 3px 3px transparent, 2px 2px 2em #851c8f;
  color: #74777a;
  background-color: #d9e2ec;
  font-family: sans-serif;
  font-size: 0.7rem;
  font-weight: 500;
  padding: 6px 10px 10px 10px;
  margin-right: 25px;
  height: 25px;
  width: 80px;

  &:hover {
    box-shadow: 3px 3px transparent, 2px 2px 2em #9d58eb;
  }
`;
const DoneButton = styled("button")`
  border: none;
  border-radius: 20px;
  box-shadow: 3px 3px transparent, 2px 2px 2em #610769;
  color: #d9e2ec;
  background-color: #ee6cf0;
  font-family: sans-serif;
  font-size: 0.7rem;
  font-weight: 500;
  padding: 6px 10px 10px 10px;
  margin-left: 25px;
  height: 25px;
  width: 80px;

  &:hover {
    box-shadow: 3px 3px transparent, 2px 2px 2em #9d58eb;
  }
`;

const GCPDatetableService = {
    async getSchedule(): Promise<any> {
        try {
            const response = await axiosApiInstance.get("https://us-central1-fullstack-development-337116.cloudfunctions.net/getSchedule");
            return response.data;
        } catch (error) {
            const gcpError = error as AxiosError;
            if (gcpError.response) {
                console.log("The following error occurred on getSchedule() call: ", gcpError.response.data);
            }
            throw error
        }
    },
    async createSchedule(startDate: any, endDate: any): Promise<any> {
        try {
            const response = await axiosApiInstance.post("https://us-central1-fullstack-development-337116.cloudfunctions.net/postSchedule",
                {
                    "startDate": startDate,
                    "endDate": endDate
                });
            return response.data;
        } catch (error) {
            const gcpError = error as AxiosError
            if (gcpError.response) {
                console.log("The following error occurred on postSchedule() call: ", gcpError.response.data);
            }
            throw gcpError;
        }
    }
}

function Datepicker(this: any, props: any) {
    const CURRENT_YEAR = +new Date().getFullYear();
    const CURRENT_MONTH = +new Date().getMonth() + 1;

    const awaitGetSchedule = async () => {
        await GCPDatetableService.getSchedule().then((value) => props.setTableResponse(value));
    }

    const WEEK_DAYS = {
        Sunday: "Su",
        Monday: "Mo",
        Tuesday: "Tu",
        Wednesday: "We",
        Thursday: "Th",
        Friday: "Fr",
        Saturday: "Sa",
    };

    const CALENDAR_MONTHS = {
        January: "Jan",
        February: "Feb",
        March: "Mar",
        April: "Apr",
        May: "May",
        June: "Jun",
        July: "Jul",
        August: "Aug",
        September: "Sep",
        October: "Oct",
        November: "Nov",
        December: "Dec",
    };

    const MONTHS_ARRAY = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];

    const CALENDAR_WEEKS = 6;
    const zeroPadding = (value: any, length: any) => {
        return `${value}`.padStart(length, "0");
    };
    const getDaysInMonth = (month = CURRENT_MONTH, year = CURRENT_YEAR) => {
        const thirtyMonths = [4, 6, 9, 11];
        const leapYear = year % 4 === 0;

        return month === 2
            ? leapYear
                ? 29
                : 28
            : thirtyMonths.includes(month)
                ? 30
                : 31;
    };
    const getFirstDayOfMonth = (month = CURRENT_MONTH, year = CURRENT_YEAR) => {
        return +new Date(`${year}-${zeroPadding(month, 2)}-01`).getUTCDay();
    };

    console.log("UTC day: ", (new Date()).getUTCDay());

    const [currentDays, setCurrentDays] = useState(getDaysInMonth);
    const [days, setDays] = useState(Array(currentDays).fill((0)));

    const isValidDate = (date: any) => {
        const isValidDate =
            Object.prototype.toString.call(date) === "[object Date]";
        const isValid = date && !Number.isNaN(date.valueOf());

        return isValidDate && isValid;
    };

    const isSameMonth = (date: any, basedate = new Date()) => {
        if (!(isValidDate(date) && isValidDate(basedate))) return false;

        const basedateMonth = +basedate.getMonth() + 1;
        const basedateYear = basedate.getFullYear();

        const dateMonth = +date.getMonth() + 1;
        const dateYear = date.getFullYear();

        return +basedateMonth === +dateMonth && +basedateYear === +dateYear;
    };

    const isSameDay = (date: any, basedate = new Date()) => {
        if (!(isValidDate(date) && isValidDate(basedate))) return false;

        const basedateDate = basedate.getDate();
        const basedateMonth = +basedate.getMonth() + 1;
        const basedateYear = basedate.getFullYear();

        const dateDate = date.getDate();
        const dateMonth = +date.getMonth() + 1;
        const dateYear = date.getFullYear();

        return (
            +basedateDate === +dateDate &&
            +basedateMonth === +dateMonth &&
            +basedateYear === +dateYear
        );
    };

    const getDateISOFormat = (date = new Date()) => {
        if (!isValidDate(date)) {
            return null;
        }

        return [
            date.getFullYear(),
            zeroPadding(+date.getMonth() + 1, 2),
            zeroPadding(+date.getDate(), 2),
        ].join("-");
    };

    const getPreviousMonth = (month: any, year: any) => {
        const previousMonth = month > 1 ? month - 1 : 12;
        const previousMonthPreviousYear = month > 1 ? year : year - 1;

        return {month: previousMonth, year: previousMonthPreviousYear};
    };

    const getNextMonth = (month: any, year: any) => {
        const nextMonth = month < 12 ? month + 1 : 1;
        const nextMonthYear = month < 12 ? year : year + 1;

        return {month: nextMonth, year: nextMonthYear};
    };

    const monthDays = getDaysInMonth(CURRENT_MONTH, CURRENT_YEAR);
    const firstDayInMonth = getFirstDayOfMonth(CURRENT_MONTH, CURRENT_YEAR);

    console.log("first day in month: ", firstDayInMonth);

    const daysFromPrevMonth = firstDayInMonth - 1;
    const daysFromNextMonth =
        CALENDAR_WEEKS * 7 - (daysFromPrevMonth + monthDays);

    const {month: previousMonth, year: previousMonthPreviousYear} =
        getPreviousMonth(CURRENT_MONTH, CURRENT_YEAR);
    const {month: nextMonth, year: nextMonthYear} = getNextMonth(
        CURRENT_MONTH,
        CURRENT_YEAR
    );

    const previousMonthDays = getDaysInMonth(
        previousMonth,
        previousMonthPreviousYear
    );

    const previousMonthDates = [...new Array(daysFromPrevMonth)].map(
        (n, index) => {
            const day = index + 1 + (previousMonthDays - daysFromPrevMonth);
            return [
                previousMonthPreviousYear,
                zeroPadding(previousMonth, 2),
                zeroPadding(day, 2),
            ];
        }
    );

    const currentMonthDates = [...new Array(monthDays)].map((n, index) => {
        const day = index + 1;
        return [CURRENT_YEAR, zeroPadding(CURRENT_MONTH, 2), zeroPadding(day, 2)];
    });

    console.log("Current Month dates: ", currentMonthDates);

    const date = new Date();
    const [month, setMonth] = useState(CURRENT_MONTH); //month is allocated from 1
    const [year, setYear] = useState(CURRENT_YEAR);
    const [selectionParity, setSelectionParity] = useState(0)
    const [startDate, setStartDate] = useState(0);
    const [endDate, setEndDate] = useState(0);

    const handleMonthSelection = (newMonth: any) => {
        setMonth(newMonth);
        setCurrentDays(getDaysInMonth(month));
        setDays(Array(currentDays).fill(0));
    }

    const handleYearSelection = (newYear: any) => {
        setYear(newYear);
        setCurrentDays(getDaysInMonth(month, year));
        setDays(Array(currentDays).fill(0));
    }

    const handleDateSelection = (day: any) => {
        setSelectionParity((selectionParity + 1) % 2)
        if(selectionParity % 2 == 0) {
            setStartDate(day);
            if(startDate > endDate) {
                let aux = startDate;
                setStartDate(endDate);
                setEndDate(aux);
            }
        } else {
            setEndDate(day);
            if(startDate > endDate) {
                let aux = startDate;
                setStartDate(endDate);
                setEndDate(aux);
            }
        }
    }

    useEffect(() => {
        console.log("Selected Month: ", month);
        console.log("Selected Year:", year);
        console.log("Start date:", startDate);
        console.log("End date:", endDate);
    }, [month, year, startDate, endDate]);

    return (
        <>
            <div className="date-selector">
                <div className="date-container">
                    <Datebar currentDay={date.getDate()}></Datebar>
                </div>
                <div className="date-container">
                    <MonthDropdown
                        currentMonth={handleMonthSelection}
                    ></MonthDropdown>
                </div>
                <div className="date-container">
                    <YearDropdown currentYear={handleYearSelection}></YearDropdown>
                </div>
            </div>
            <div className="calendar-body">
                <div className="calendar">
                    <div className="day">
                        <div>Su</div>
                        <div>Mo</div>
                        <div>Tu</div>
                        <div>We</div>
                        <div>Th</div>
                        <div>Fr</div>
                        <div>Sa</div>
                    </div>
                    <div className="date-grid">
                        {days.map((_nr, index) => (
                            <button key={index} onClick={() => handleDateSelection(index+1)}>
                                <time dateTime={`${year}-${month}-0${index + 1}`}>{index + 1}</time>
                            </button>
                        ))}
                    </div>
                </div>
            </div>
            <div className="selection">
                <ButtonContainer>
                    <CancelButton>CANCEL</CancelButton>
                    <DoneButton onClick={() => GCPDatetableService.createSchedule(startDate, endDate).then(awaitGetSchedule)}>DONE</DoneButton>
                </ButtonContainer>
            </div>
        </>
    );
}

export default Datepicker;
