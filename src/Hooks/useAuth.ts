import {AxiosError} from 'axios';
import axiosApiInstance from "../Api/ApiInstance";

const userService = {
    async login(email: string, password: string): Promise<any> {
        const payload = JSON.stringify({email, password});
        console.log(payload);
        try {
            const response = await axiosApiInstance.post(`http://localhost:3001/login`, payload);
            console.log(response.data);
            return response.data;
        } catch (error) {
            if ((error as AxiosError).response) {
                let loginError = (error as AxiosError).response
                console.log(
                    "Error occurred at login!\n status: ",
                    loginError?.status,
                    "\nbody: ",
                    loginError?.data
                )
            }
            throw error;
        }
    },
    logout() {
        localStorage.removeItem("currentUser");
        localStorage.removeItem("authToken");
    }
}

export default userService;