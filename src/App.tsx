import "./App.scss";
import {useState} from 'react';
import {Switch, BrowserRouter as Router} from "react-router-dom";
import AppRoutes from "./Components/AppRoutes";
import appRoutes from "./routes";
import { createBrowserHistory } from 'history';
import AuthContext from "./Context/AuthContext";
import axiosTokenInterceptor from "./Api/AxiosInterceptor";

function App() {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const pagesHistory = createBrowserHistory();
    axiosTokenInterceptor.setAxiosInterceptor(pagesHistory);

    return (
       <AuthContext.Provider value={isAuthenticated}>
           <Router>
               <Switch>
                   {
                       appRoutes.map((route) => (
                       <AppRoutes
                           key={route.path}
                           path={route.path}
                           component={route.component}
                           isPrivate={route.isPrivate}
                       />
                   ))}
               </Switch>
           </Router>
       </AuthContext.Provider>
    );
}

export default App;