import axiosApiInstance from "./ApiInstance";

const axiosTokenInterceptor = {
    setAxiosInterceptor: (previousSession: any) => {
        axiosApiInstance.interceptors.request.use(
            async(reqConfig: any) => {
                const authToken = localStorage.getItem("authToken");
                const previousSessionUser = localStorage.getItem("currentUser");
                if(authToken && !(reqConfig.url?.includes("/") || reqConfig.url?.includes("/register"))) {
                    reqConfig.headers = {
                        authToken: `Bearer: ${authToken}`
                    };
                    console.log(authToken);
                }
                return reqConfig;
            },
            (error) => Promise.reject(error)
        );
        axiosApiInstance.interceptors.response.use(
            (response) => response,
            async(err) => {
                previousSession.push("/");
                return Promise.reject(err);
            });
    }
}


export default axiosTokenInterceptor;