import dotenv from 'dotenv';
import axios from 'axios';

dotenv.config();
const {REACT_APP_BASE_URL} = process.env // ReactDocs REACT_APP prefix to global env files

const options = {
    baseUrl: REACT_APP_BASE_URL,
    timeout: 15000,
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        "Access-Control-Allow-Headers": "Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control",
        "Access-Control-Allow-Methods": "OPTIONS, GET, POST",
    },
};

const axiosApiInstance = axios.create(options);

export default axiosApiInstance;